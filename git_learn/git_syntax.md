# Git 命令
## 常用命令
- git init    初始化仓库
- git add .    添加所有修改文件到暂不区，如果添加指定文件把点换成文件名即可
- git add *.txt    添加所有txt文件到暂存区
- git add <dir>/*    递归添加指定目录下所有文件
- git add --update    将距上次修改过的文件添加到暂存区
- git add -p <file_name>    部分暂存，将依次询问每个更改
- git add --interactive    交互式提交暂存,短命令-i
- git commit --interactive    交互式commit
- git commit -m 'xxx'    将暂存区的文件提交，并添加备注xxx。如果不加-m选项，则会自动打开默认编辑器，-m是--message的缩写
- git commit -am 'xxx'    先暂存再提交，相当于同时进行了add步骤，从未add过的文件不可用
- git commit --amend    修正提交，使用当前暂存的修改更新之前的提交，并提供一个新的提交消息，加--no-edit可以跳过编辑注释，加--reset-author可以修正作者信息
- git commit -t <template_file>    指定一个模版文件，用于提示提交时该写什么
- git config --global commit.template<template file location>    配置全局模版位置
- git commit --verbose    在写注释时提供diff信息以供参考，当提供两个--verbose时会同时提供暂存区域和工作目录的diff
- git reset filename    取消将某文件添加到暂存区
- git staus    查看git仓库的当前状态
- git log --oneline    每行一条查看日志，不加--oneline可查看详细日志
- git log --graph    查看图形化历史
- git reflog    列出本地仓库的完整历史记录
- git show <sha>    查看某一次提交的信息，sha可简写，最少前4位
- git clean -f    清理未被追踪的文件
- git notes add -m 'xxx' <sha>    为<sha>添加注释(不是备注消息)，可以用 git show <sha> 来查看

### git的模式信息
- 040000: 目录
- 100644: 常规的不可执行文件
- 100755: 常规的可执行文件
- 120000: 符号链接
- 160000: gitlink


## 配置命令
- git config --global user.name    配置用户名，范围可选 local/global/system
- git config --global user.email    配置用户邮箱，范围同上，默认 local
- git config --global core.excludesfile ~/.gitignore    配置全局排除文件
- git忽略文件每个目录都可以有一个
- git忽略文件写入.git/info/exclude文件时，该文件不会被保存在仓库中，但有效没有工作目录中.gitignore的优先度高

## git属性文件
- git有一个处理特定文件类型的方式，它会自动判断哪些文件是不是二进制文件，而我们则可以指定一个属性文件来指明怎么处理哪些文件。
- git属性文件有不同的生效范围，其中.git/info/attributes文件如果存在则优先使用，其次是每个文件夹下的.gitattributes文件，再往后就是上级文件夹内的属性名。
格式示例|含义
--|--
*.obj binary|所有obj文件都被视为二进制文件
*.java -crlf|非java应当具有CRLF
eol=crlf|在签出时为匹配文件将行结束符设置为CRLF
eol=lf|在新增或提交时标准化文件(为LF)，并且在签出时保留LF

## 分支相关命令
- git branch xxx    新建分支xxx
- git switch xxx    切换到xxx分支
- git switch -    切换到上一个分支
- git merge xxx    将xxx分支合并到当前分支
- git merget -ff xxx   尝试进行快进式合并，--no-ff不要进行快进式合并，--ff-only，仅在可使用快进的时候合并
- git cherry-pick <sha>    樱桃挑选，指定<sha>值
- git cherry-pick <sha1>..<sha2>    选取从<sha1>(不含)到<sha2>(含)之间的提交
- git fetch xxx    获取更新的列表和所有远程分支的内容
- git branch --list    列出所有本地分支
- git branch --remotes    列出远程分支
- git branch --all    列出所有分支
- git branch -av    列出所有分支和当前指向
- git push --delete <remote_name> <branch_remote>    删除远程服务器中指定名称的分支
- git rebase <branch_name>    变基，将<branch_name>的提交合并到当前分支
- git rebase --continue    解决冲突后重新提交变基
- git rebase --skip    放弃冲突的本分支的提交
- git rebase --abort    取消本次变基
- git checkout <sha>    签出提交
- git checkout -- <file_name>    当误删文件时，可以签出文件以恢复
- git reset head <file_name>    当误删文件并添加暂存区时，需要先重置head才能checkout
- git reset --hard head    撤消所有更改，恢复到上一次提交(soft仅更新本地仓库的HEAD，默认mixed更新本地仓库和暂存区的HEAD，hard更新本地仓库/暂存区/工作目录的HEAD)
- git reset --soft HEAD^^^
- git reset --soft HEAD~3    在一个引用后插入^意味着之前的一个，使用~#亦同，本例是之前的3个提交
- git commit --amend    合并到上一次提交(会生成新的sha)
- git revert HEAD^^^    消除变更到HEAD之前的3次提交，也就是做3次反向更改然后提交，不会删除任何已有提交

## 标签相关命令
- git tag -a <tag_name> -m <memo>    添加附注标签
- git tag <tag_name>    添加轻量级标签
- git tag -a <tag_name> <sha>  后期补标签，sha是指定校验和
- git tag -f <tag_name> <new_sha>  修改<tag_name>指向
- git tag -d <tag_name>    删除<tag_name>

## 远程仓库相关命令
- git remote add xxx url    添加地址为url的远程仓库，并命名为xxx。这个xxx可以随便起，后面pull/push会用到
- git pull xxx master    将xxx远程仓库的master内容拉到本地
- git push xxx master    将本地内容推送到xxx远程仓库的master分支，加上--tags同时推送标签
- git remote rename <old_name> <new_name>    更改远程仓库在本地对应的名字
- git clone url    下载地址为URL的远程仓库的一个副本
- git push --all    推送所有分支
- git push --delete <nickname> <branch>    删除<nickname>上<branch>分支，或者使用 git push --delete <nickname>:<branch>
- git push -f <nickname> <branch>    强制推送
- git fetch <nickname> <branch>    更新远程分支的最新更新，fetch + merge = pull
- start是收藏，watched是关注会收到项目变更信息，fork是分叉。fork之后会获得项目副本，你可以对项目进行更改，然后向原项目发起<拉取请求>来贡献自己的代码。

## 工作树
- git worktree add <New_wk_name>    新建一个工作树，其实<New_wk_name>也就是一个分支名，而新工作树则像是与当前分支同时存在的分支
- git worktree remove <wk_name>    删除一个工作树
- git worktree list    列出所有的工作树
- 新建出的工作树是不能使用switch切换的，因为它是与当前分支同是物理存在的，已经处于签出状态！如要切换到工作树，cd到相应目录即可。各工作树之间拥有不同的工作目录和暂存区，但指向的是相同的本地仓库。 
- git submodule status    查看子模块状态

## gitee.com免密提交
注：使用相应的免密提交方法需要相应远程的URL地址，如使用公钥则设置URL为git协议，否则无效
### <1>使用https协议，在本地存储密码
1. vim .git-credentials 编辑内容为: https://username:password@gitee.com
2. 执行 git config --global  credential.helper <setting_name>    //添加配置
3. 第一次提交的时候仍会要求输入密码，以后就不用了
4. 如需取消配置执行 git config --global --unset credential.helper <setting_name>    //删除配置
### <2>使用git协议+ssh公钥授权
1. 需要先在本地生成公私钥
2. 将公钥添加到gitee帐户中

# Git 相关文档/网址
- [git 官网 git-scm.com/](https://git-scm.com/)
- [Git Pro 中文版 gitee.com/progit/](https://gitee.com/progit/)

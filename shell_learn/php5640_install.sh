# download php
wget http://mirrors.sohu.com/php/php-5.6.40.tar.bz2

# ln libmysqlclient.so.20 to libmysqlclient_r.so
ln -s /usr/local/mysql/lib/libmysqlclient.so.20 /usr/local/mysql/lib/libmysqlclient_r.so

# do
tar jxvf php-5.6.40.tar.bz2
cd php-5.6.40
./configure \
	--prefix=/usr/local/php \
	--with-apxs2=/usr/local/apache2/bin/apxs \
	--with-mysql=/usr/local/mysql \
	--with-mysqli=/usr/local/mysql/bin/mysql_config \
	--with-pdo-mysql=/usr/local/mysql/bin/mysql_config \

make
make install
cp php.ini-production /usr/local/php/lib/php.ini
cd ..

# clean php
rm -rf php-5.6.40*

# configure apache2
sed -i "390 i \ \ \ \ AddType application/x-httpd-php .php" /usr/local/apache2/conf/httpd.conf
echo "<?php phpinfo(); ?>" > /usr/local/apache2/htdocs/index.php
/usr/local/apache2/bin/apachectl -k restart

echo "-----------------------------------------"
echo "-------------php installed---------------"
echo "-----------------------------------------"

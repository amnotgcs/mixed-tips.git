# download php-5.6.50
wget http://mirrors.sohu.com/php/php-5.6.40.tar.bz2
tar jxvf php-5.6.40.tar.bz2
cd php-5.6.40/ext/
ext_path=$(pwd)
ln -s $ext_path $ext_path/pdo_mysql/
ln -s $ext_path $ext_path/mysql/
apt install autoconf -y

# install gd.so
cd gd
apt install libpng-dev
/usr/local/php/bin/phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make
make install
sed -i "911 a extension=gd.so" /usr/local/php/lib/php.ini
/usr/local/apache2/bin/apachectl -k restart
cd ..

# install libmsyql
cd mysql
/usr/local/php/bin/phpize
./configure \
	--with-php-config=/usr/local/php/bin/php-config \
	--with-mysql=mysqlnd

make
make install
sed -i "911 a extension=mysql.so" /usr/local/php/lib/php.ini
/usr/local/apache2/bin/apachectl -k restart
cd ..

# install pdo_mysql
cd pdo_mysql
/usr/local/php/bin/phpize
./configure \
	--with-php-config=/usr/local/php/bin/php-config \
	--with-mysql=mysqlnd

make 
make install
sed -i "911 a extensionn=pdo_mysql.so" /usr/local/php/lib/php.ini
/usr/local/apache2/bin/apachectl -k restart
cd ..

cd ../../
rm -rf php-5.6.40*


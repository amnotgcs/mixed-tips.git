#----------------------------------------
#----auto install mysql5.7---------------
#----------------------------------------
#----aurhor: amnotgcs--------------------
#----------------------------------------
#----time: 2020-12-17 -------------------
#----------------------------------------
# instruction
# 1. download boost and install
# 2. download mysql and install
# 3. start mysql and reset password
# 4. clean remains files


# 1.1 download boost
# offical website=https://www.boost.org/users/download/
# if download with error, consider change the download link
rm boost_1_59_0*
wget https://nchc.dl.sourceforge.net/project/boost/boost/1.59.0/boost_1_59_0.tar.gz

# 1.2 extra boost and install
tar zxvf boost_1_59_0.tar.gz
cd boost_1_59_0
./bootstrap.sh
./b2 install
cd ..
echo "----------boost_1_59_installed---------"

# 2.1 download mysql5.7
rm mysql-5.7.32*
wget https://cdn.mysql.com//Downloads/MySQL-5.7/mysql-5.7.32.tar.gz

# 2.2 extra mysql5.7 and install
tar zxvf mysql-5.7.32.tar.gz
cd mysql-5.7.32
pkill --signal 9 apt
rm /var/lib/dpkg/lock

apt install cmake -y
apt install libaio-dev -y
apt install libssh-dev -y
apt install libncurses-dev -y

mkdir bld
cd bld
make clean
rm CMakeCache.txt

cmake .. -DBUILD_CONFIG=mysql_release \
	-DCPACK_MONOLITHIC_INSTALL=ON \
	-DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
	-DDEFAULT_CHARSET=utf8 \
	-DDEFAULT_COLLATION=utf8_general_ci \
	-DMYSQLX_TCP_PORT=33060 \
	-DMYSQL_UNIX_ADDR=/usr/local/mysql/mysql.sock \
	-DMYSQL_TCP_PORT=3306 \
	-DMYSQLX_UNIX_ADDR=/usr/local/mysql/mysqlx.sock \
	-DMYSQL_DATADIR=/usr/local/mysql/data \
	-DSYSCONFDIR=/usr/local/mysql/etc \
	-DENABLE_DOWNLOADS=ON \
	-DWITH_BOOST=system

make -j4
make install

# 2.3 create mysql user&group
groupadd mysql
useradd -r -g mysql -s /bin/false mysql

# 2.4 set privilege for mysql directory
chown -R mysql:mysql /usr/local/mysql

# 3.1 initialize mysql
pkill --signal 9 mysql
rm -rf /usr/local/mysql/data
echo "----------------------------------------"
echo "    initializing...."
echo "----------------------------------------"
/usr/local/mysql/bin/mysqld --initialize --user=mysql > /tmp/password 2>&1;
tail -n 1 /tmp/password


# 3.2 enable ssl
/usr/local/mysql/bin/mysql_ssl_rsa_setup

# 3.3 start mysql and reset password
password=$(tail -n 1 /tmp/password | sed "s/.*localhost: //g")
# /usr/local/mysql/bin/mysqld_safe --user=mysql >/dev/null 2>&1;
/usr/local/mysql/support-files/mysql.server start

target_password="1233"
changePWD="alter user 'root'@'localhost' identified by '$target_password';"
flushPV="flush privileges;"
quit="exit;"
cmd="$changePWD;$flushPV;"
/usr/local/mysql/bin/mysql -uroot -p$password --connect-expired-password -e "${cmd}"

# 3.4 set path
echo "export PATH=\$PATH:/usr/local/mysql/bin" >> /etc/profile
source /etc/profile

# 3.5 info
echo "----------------------------------------"
echo "    origin_password:    $password"
echo "-----------------------------------------"
echo "    now_password:       $target_password"
echo "-----------------------------------------"

# 4.1 clean remains files
cd ../../
rm -rf boost_1_59_0*
rm -rf mysql-5.7.32*


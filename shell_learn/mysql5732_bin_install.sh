# download compressed tar archive
wget https://cdn.mysql.com//Downloads/MySQL-5.7/mysql-5.7.32-linux-glibc2.12-x86_64.tar.gz

# create mysql user&group
groupadd mysql
useradd -r -g mysql -s /bin/false mysql

# Uncompress archive to /usr/local
tar zxvf mysql-5.7.32-linux-glibc2.12-x86_64.tar.gz -C /usr/local/
mv /usr/local/mysql-5.7.32-linux-glibc2.12-x86_64 /usr/local/mysql 
chown -R mysql:mysql mysql

# set privilege for mysql directory
chown -R mysql:mysql /usr/local/mysql

# initialize mysql
pkill --signal 9 mysql
rm -rf /usr/local/mysql/data
echo "----------------------------------------"
echo "    initializing...."
echo "----------------------------------------"
/usr/local/mysql/bin/mysqld --initialize --user=mysql > /tmp/password 2>&1;
tail -n 1 /tmp/password


# enable ssl
/usr/local/mysql/bin/mysql_ssl_rsa_setup

# start mysql and reset password
password=$(tail -n 1 /tmp/password | sed "s/.*localhost: //g")
# /usr/local/mysql/bin/mysqld_safe --user=mysql >/dev/null 2>&1;
/usr/local/mysql/support-files/mysql.server start

target_password="1233"
changePWD="alter user 'root'@'localhost' identified by '$target_password';"
flushPV="flush privileges;"
quit="exit;"
cmd="$changePWD;$flushPV;"
/usr/local/mysql/bin/mysql -uroot -p$password --connect-expired-password -e "${cmd}"

# set path
echo "export PATH=\$PATH:/usr/local/mysql/bin" >> /etc/profile
echo "export PATH=\$PATH:/usr/local/mysql/support-files/" >> /etc/profile
souce /etc/profile
source ~/.bashrc

# info
echo "----------------------------------------"
echo "    origin_password:    $password"
echo "-----------------------------------------"
echo "    now_password:       $target_password"
echo "-----------------------------------------"

